What about the quality of code with rules ?

```sh

tp@tp-VM:~/tp/tp1_ex3$ pip3 install pylint --user
Collecting pylint
  Downloading https://files.pythonhosted.org/packages/e9/59/43fc36c5ee316bb9aeb7cf5329cdbdca89e5749c34d5602753827c0aa2dc/pylint-2.4.4-py3-none-any.whl (302kB)
    100% |████████████████████████████████| 307kB 119kB/s 
Collecting mccabe<0.7,>=0.6 (from pylint)
  Downloading https://files.pythonhosted.org/packages/87/89/479dc97e18549e21354893e4ee4ef36db1d237534982482c3681ee6e7b57/mccabe-0.6.1-py2.py3-none-any.whl
Collecting astroid<2.4,>=2.3.0 (from pylint)
  Downloading https://files.pythonhosted.org/packages/ad/ae/86734823047962e7b8c8529186a1ac4a7ca19aaf1aa0c7713c022ef593fd/astroid-2.3.3-py3-none-any.whl (205kB)
    100% |████████████████████████████████| 215kB 3.1MB/s 
Collecting isort<5,>=4.2.5 (from pylint)
  Downloading https://files.pythonhosted.org/packages/e5/b0/c121fd1fa3419ea9bfd55c7f9c4fedfec5143208d8c7ad3ce3db6c623c21/isort-4.3.21-py2.py3-none-any.whl (42kB)
    100% |████████████████████████████████| 51kB 3.9MB/s 
Collecting typed-ast<1.5,>=1.4.0; implementation_name == "cpython" and python_version < "3.8" (from astroid<2.4,>=2.3.0->pylint)
  Downloading https://files.pythonhosted.org/packages/90/ed/5459080d95eb87a02fe860d447197be63b6e2b5e9ff73c2b0a85622994f4/typed_ast-1.4.1-cp36-cp36m-manylinux1_x86_64.whl (737kB)
    100% |████████████████████████████████| 747kB 1.1MB/s 
Collecting wrapt==1.11.* (from astroid<2.4,>=2.3.0->pylint)
  Downloading https://files.pythonhosted.org/packages/23/84/323c2415280bc4fc880ac5050dddfb3c8062c2552b34c2e512eb4aa68f79/wrapt-1.11.2.tar.gz
Collecting lazy-object-proxy==1.4.* (from astroid<2.4,>=2.3.0->pylint)
  Downloading https://files.pythonhosted.org/packages/0b/dd/b1e3407e9e6913cf178e506cd0dee818e58694d9a5cd1984e3f6a8b9a10f/lazy_object_proxy-1.4.3-cp36-cp36m-manylinux1_x86_64.whl (55kB)
    100% |████████████████████████████████| 61kB 183kB/s 
Collecting six~=1.12 (from astroid<2.4,>=2.3.0->pylint)
  Downloading https://files.pythonhosted.org/packages/65/eb/1f97cb97bfc2390a276969c6fae16075da282f5058082d4cb10c6c5c1dba/six-1.14.0-py2.py3-none-any.whl
Building wheels for collected packages: wrapt
  Running setup.py bdist_wheel for wrapt ... done
  Stored in directory: /home/tp/.cache/pip/wheels/d7/de/2e/efa132238792efb6459a96e85916ef8597fcb3d2ae51590dfd
Successfully built wrapt
Installing collected packages: mccabe, typed-ast, wrapt, lazy-object-proxy, six, astroid, isort, pylint
Successfully installed astroid-2.3.3 isort-4.3.21 lazy-object-proxy-1.4.3 mccabe-0.6.1 pylint-2.4.4 six-1.14.0 typed-ast-1.4.1 wrapt-1.11.2
tp@tp-VM:~/tp/tp1_ex3$ pylint Ex3.py 

La commande « pylint » n'a pas été trouvée, mais peut être installée avec :

sudo apt install pylint

tp@tp-VM:~/tp/tp1_ex3$ python -m pylint Ex3.py 
************* Module Ex3
Ex3.py:7:23: C0326: No space allowed before bracket
    def sum(self, a, b ):
                       ^ (bad-whitespace)
Ex3.py:8:0: W0301: Unnecessary semicolon (unnecessary-semicolon)
Ex3.py:11:0: W0301: Unnecessary semicolon (unnecessary-semicolon)
Ex3.py:14:0: W0301: Unnecessary semicolon (unnecessary-semicolon)
Ex3.py:17:0: W0301: Unnecessary semicolon (unnecessary-semicolon)
Ex3.py:22:1: C0326: Exactly one space required around assignment
a=5
 ^ (bad-whitespace)
Ex3.py:23:1: C0326: Exactly one space required around assignment
b=7
 ^ (bad-whitespace)
Ex3.py:24:1: C0326: Exactly one space required around assignment
s=SimpleCalculator()
 ^ (bad-whitespace)
Ex3.py:25:6: C0326: No space allowed before bracket
print ("We will test  5+7, 5-7, 5*7 et 5/7");
      ^ (bad-whitespace)
Ex3.py:25:0: W0301: Unnecessary semicolon (unnecessary-semicolon)
Ex3.py:26:13: C0326: Exactly one space required after comma
print(s.sum(a,b));
             ^ (bad-whitespace)
Ex3.py:26:0: W0301: Unnecessary semicolon (unnecessary-semicolon)
Ex3.py:27:19: C0326: Exactly one space required after comma
print(s.substract(a,b));
                   ^ (bad-whitespace)
Ex3.py:27:0: W0301: Unnecessary semicolon (unnecessary-semicolon)
Ex3.py:28:18: C0326: Exactly one space required after comma
print(s.multiply(a,b));
                  ^ (bad-whitespace)
Ex3.py:28:0: W0301: Unnecessary semicolon (unnecessary-semicolon)
Ex3.py:29:16: C0326: Exactly one space required after comma
print(s.divide(a,b));
                ^ (bad-whitespace)
Ex3.py:29:0: W0301: Unnecessary semicolon (unnecessary-semicolon)
Ex3.py:31:0: C0303: Trailing whitespace (trailing-whitespace)
Ex3.py:33:0: C0305: Trailing newlines (trailing-newlines)
Ex3.py:1:0: C0103: Module name "Ex3" doesn't conform to snake_case naming style (invalid-name)
Ex3.py:1:0: C0114: Missing module docstring (missing-module-docstring)
Ex3.py:5:0: C0115: Missing class docstring (missing-class-docstring)
Ex3.py:7:18: W0621: Redefining name 'a' from outer scope (line 22) (redefined-outer-name)
Ex3.py:7:21: W0621: Redefining name 'b' from outer scope (line 23) (redefined-outer-name)
Ex3.py:7:4: C0103: Argument name "a" doesn't conform to snake_case naming style (invalid-name)
Ex3.py:7:4: C0103: Argument name "b" doesn't conform to snake_case naming style (invalid-name)
Ex3.py:7:4: C0116: Missing function or method docstring (missing-function-docstring)
Ex3.py:7:4: R0201: Method could be a function (no-self-use)
Ex3.py:10:24: W0621: Redefining name 'a' from outer scope (line 22) (redefined-outer-name)
Ex3.py:10:27: W0621: Redefining name 'b' from outer scope (line 23) (redefined-outer-name)
Ex3.py:10:4: C0103: Argument name "a" doesn't conform to snake_case naming style (invalid-name)
Ex3.py:10:4: C0103: Argument name "b" doesn't conform to snake_case naming style (invalid-name)
Ex3.py:10:4: C0116: Missing function or method docstring (missing-function-docstring)
Ex3.py:10:4: R0201: Method could be a function (no-self-use)
Ex3.py:13:23: W0621: Redefining name 'a' from outer scope (line 22) (redefined-outer-name)
Ex3.py:13:26: W0621: Redefining name 'b' from outer scope (line 23) (redefined-outer-name)
Ex3.py:13:4: C0103: Argument name "a" doesn't conform to snake_case naming style (invalid-name)
Ex3.py:13:4: C0103: Argument name "b" doesn't conform to snake_case naming style (invalid-name)
Ex3.py:13:4: C0116: Missing function or method docstring (missing-function-docstring)
Ex3.py:13:4: R0201: Method could be a function (no-self-use)
Ex3.py:16:21: W0621: Redefining name 'a' from outer scope (line 22) (redefined-outer-name)
Ex3.py:16:24: W0621: Redefining name 'b' from outer scope (line 23) (redefined-outer-name)
Ex3.py:16:4: C0103: Argument name "a" doesn't conform to snake_case naming style (invalid-name)
Ex3.py:16:4: C0103: Argument name "b" doesn't conform to snake_case naming style (invalid-name)
Ex3.py:16:4: C0116: Missing function or method docstring (missing-function-docstring)
Ex3.py:16:4: R0201: Method could be a function (no-self-use)
Ex3.py:22:0: C0103: Constant name "a" doesn't conform to UPPER_CASE naming style (invalid-name)
Ex3.py:23:0: C0103: Constant name "b" doesn't conform to UPPER_CASE naming style (invalid-name)
Ex3.py:24:0: C0103: Constant name "s" doesn't conform to UPPER_CASE naming style (invalid-name)

-------------------------------------
Your code has been rated at -19.41/10

tp@tp-VM:~/tp/tp1_ex3$ 


 

 Not so good !
 
  ```sh
 We can try first black :
 
 
  pip3 install --user black 
Collecting black
  Downloading https://files.pythonhosted.org/packages/fd/bb/ad34bbc93d1bea3de086d7c59e528d4a503ac8fe318bd1fa48605584c3d2/black-19.10b0-py36-none-any.whl (97kB)
    100% |████████████████████████████████| 102kB 529kB/s 
Collecting attrs>=18.1.0 (from black)
  Downloading https://files.pythonhosted.org/packages/a2/db/4313ab3be961f7a763066401fb77f7748373b6094076ae2bda2806988af6/attrs-19.3.0-py2.py3-none-any.whl
Collecting appdirs (from black)
  Downloading https://files.pythonhosted.org/packages/56/eb/810e700ed1349edde4cbdc1b2a21e28cdf115f9faf263f6bbf8447c1abf3/appdirs-1.4.3-py2.py3-none-any.whl
Collecting pathspec<1,>=0.6 (from black)
  Downloading https://files.pythonhosted.org/packages/34/fa/c5cc4f796eb954b56fd1f6c7c315647b18b027e0736c9ae87b73bbb1f933/pathspec-0.7.0-py2.py3-none-any.whl
Collecting typed-ast>=1.4.0 (from black)
  Using cached https://files.pythonhosted.org/packages/90/ed/5459080d95eb87a02fe860d447197be63b6e2b5e9ff73c2b0a85622994f4/typed_ast-1.4.1-cp36-cp36m-manylinux1_x86_64.whl
Collecting toml>=0.9.4 (from black)
  Downloading https://files.pythonhosted.org/packages/a2/12/ced7105d2de62fa7c8fb5fce92cc4ce66b57c95fb875e9318dba7f8c5db0/toml-0.10.0-py2.py3-none-any.whl
Collecting regex (from black)
  Downloading https://files.pythonhosted.org/packages/ed/36/fd20c656fb4a4fbe8db367ea274c3465b81cb2e01ffc57b9980f0578e131/regex-2020.2.20-cp36-cp36m-manylinux1_x86_64.whl (690kB)
    100% |████████████████████████████████| 696kB 1.2MB/s 
Collecting click>=6.5 (from black)
  Downloading https://files.pythonhosted.org/packages/dd/c0/4d8f43a9b16e289f36478422031b8a63b54b6ac3b1ba605d602f10dd54d6/click-7.1.1-py2.py3-none-any.whl (82kB)
    100% |████████████████████████████████| 92kB 3.2MB/s 
Installing collected packages: attrs, appdirs, pathspec, typed-ast, toml, regex, click, black
Successfully installed appdirs-1.4.3 attrs-19.3.0 black-19.10b0 click-7.1.1 pathspec-0.7.0 regex-2020.2.20 toml-0.10.0 typed-ast-1.4.1
tp@tp-VM:~/tp/tp1_ex3$ python -m black Ex3.py 
reformatted Ex3.py
All done! ✨ 🍰 ✨
1 file reformatted.

 ```
 En finally :
 
 ```sh
 tp@tp-VM:~/tp/tp1_ex3$ pylint ex3_black_docstring.py 
************* Module ex3_black_docstring
ex3_black_docstring.py:15:4: R0201: Method could be a function (no-self-use)
ex3_black_docstring.py:28:4: R0201: Method could be a function (no-self-use)
ex3_black_docstring.py:41:4: R0201: Method could be a function (no-self-use)
ex3_black_docstring.py:55:4: R0201: Method could be a function (no-self-use)

------------------------------------------------------------------
Your code has been rated at 7.65/10 (previous run: 7.65/10, +0.00)

tp@tp-VM:~/tp/tp1_ex3$ pylint ex3_black_docstring_static.py 

--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
 ```
