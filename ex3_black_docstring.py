#!/usr/bin/env python
# coding: utf-8

"""
In this simple example,
We create a simple Class SimpleCalculator and try differents methods directlely
"""

class SimpleCalculator:
    """
    This is a class for simple mathematical operations on simple numbers.

    """

    def sum(self, var_a, var_b):
        """
        The function to sum two  Numbers.

        Parameters:
            var_a (int ): First operand
            var_b (int ): Second operand

        Returns:
            (int): A number which contains the sum.
        """
        return var_a + var_b

    def substract(self, var_a, var_b):
        """
        The function to substract two  Numbers.

        Parameters:
            var_a (int ): First operand
            var_b (int ): Second operand

        Returns:
            (int): A number which contains the product.
        """
        return var_a - var_b

    def multiply(self, var_a, var_b):
        """
        The function to multiply two  Numbers.

        Parameters:
            var_a (int ): First operand
            var_b (int ): Second operand

        Returns:
            (int): A number which contains the product.
        """

        return var_a * var_b

    def divide(self, var_a, var_b):
        """
        The function to multiply two  Numbers.

        Parameters:
            var_a (int ): First operand
            var_b (int ): Second operand

        Returns:
            (real): A complex number which contains the result of division
        """
        return var_a / var_b


### simple Test of the class ###

VAL_A = 5
VAL_B = 7
MY_CALCULATOR = SimpleCalculator()
print("We will test  5+7, 5-7, 5*7 et 5/7")
print(MY_CALCULATOR.sum(VAL_A, VAL_B))
print(MY_CALCULATOR.substract(VAL_A, VAL_B))
print(MY_CALCULATOR.multiply(VAL_A, VAL_B))
print(MY_CALCULATOR.divide(VAL_A, VAL_B))
