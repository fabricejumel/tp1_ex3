#!/usr/bin/env python
# coding: utf-8


class SimpleCalculator:
    ##### to remove ######
    def sum(self, var_a, var_b):
        return var_a + var_b

    def substract(self, var_a, var_b):
        return var_a - var_b

    def multiply(self, var_a, var_b):
        return var_a * var_b

    def divide(self, var_a, var_b):
        return var_a / var_b


############# Test functions ############
VAL_A = 5
VAL_B = 7
my_calculator = SimpleCalculator()
print("We will test  5+7, 5-7, 5*7 et 5/7")
print(my_calculator.sum(VAL_A, VAL_B))
print(my_calculator.substract(VAL_A, VAL_B))
print(my_calculator.multiply(VAL_A, VAL_B))
print(my_calculator.divide(VAL_A, VAL_B))
